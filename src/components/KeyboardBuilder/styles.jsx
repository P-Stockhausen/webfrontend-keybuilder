export default theme => ({
  root: {
    overflow: 'auto',
    flexGrow: 1,
  },
  canvas: {
    position: 'absolute',
    width: '64vw',
    height: '90vh',
  }
});