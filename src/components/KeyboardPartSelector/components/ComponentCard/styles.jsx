export default theme => ({
  root: {
    display: 'flex',
    width: '20vw',
    height: '15vh'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: '13vw'
  },
  content: {
    flex: '1 0 auto',
  },
  media: {
    width: '7vw',
  },
});