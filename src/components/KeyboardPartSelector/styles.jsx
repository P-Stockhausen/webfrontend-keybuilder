export default theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
    height: '100%',
  },
  selector:{
    position: 'absolute',
    width: '33vw',
    maxHeight: '90vh',
  },
});