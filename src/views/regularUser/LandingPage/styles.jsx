export default theme => ({
  root: {
    overflow: 'hidden',
    padding: theme.spacing(2),
    flexGrow: 1,
    height: '100vh',
    weight: '100vw',
    minHeight: '100%'
  },
  box: {
    paddingTop: '5em',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
